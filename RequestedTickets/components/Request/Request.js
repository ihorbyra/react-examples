import React from 'react';
import {Link} from 'react-router-dom';
import {FormattedDate} from "react-intl";


import './Request.scss';
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {faTrashAlt} from "@fortawesome/fontawesome-free-solid";

const request = props => {

    const {
        name: nameTicket,
        venue_id: venueId,
        city: cityName,
        venue: {
            name: venueName
        }
    } = props.request.event;

    const {
        image_url: clientImg,
        name: clientName
    } = props.request.details;

    const d = new Date(props.request.event.date);
    const day = <FormattedDate value={d} day='2-digit' />;
    const month = <FormattedDate value={d} month='short' />;
    const year = <FormattedDate value={d} year='numeric' />;
    let currencySymbol = null;
    let price = null;

    if(props.request.max_amount.origin_price){
        price = props.request.max_amount.origin_price;
    }

    if(props.request.event.country){
        if(props.request.event.country.currency_symbol) {
            currencySymbol = props.request.event.country.currency_symbol;
        }
    }

    const typeTicket = props.request.type ? props.request.type.name : `Any`;

    return (
        <div className={`row ticket-requested`}>
            <div className="col-4 col-sm-4 col-md-2 col-lg-2 col-xl-2 date-wr">
                <div className="date">
                    <div className="day">{day}</div>
                    <div className="month">{month}</div>
                    <div className="year">{year}</div>
                </div>
            </div>
            <div className="col-8 col-sm-8 col-md-7 col-lg-7 col-xl-7">
                <div className="info-event">
                    <img src={`${clientImg}`} alt={`${clientName}`} />
                    <h3 className="title">
                        <span className={`type-ticket`}>{props.request.quantity} x {typeTicket} </span>
                        {nameTicket}
                        <Link to={`/venue/${venueId}`} className="location">@{venueName}, {cityName}</Link>
                    </h3>
                </div>
                <hr />
                <div className="location">
                    <div className="title-info">Wanted ticket information</div>
                    <div className="ticket-info">{typeTicket}</div>
                </div>
            </div>
            <div className="offset-0 col-12 offset-sm-4 col-sm-8 offset-md-0 col-md-3 col-lg-3 col-xl-3 price-info">
                <div className="price-starts">Max Price</div>
                <div className="price">{currencySymbol} {price}</div>
                <Link to={`/profile/requested-tickets/${props.request.event_id}`} className="goToEvent">See responses</Link>
            </div>
            <span className="trash" style={{cursor:"pointer"}} onClick={() => props.click(props.request.id)} >
                <FontAwesomeIcon
                    icon={faTrashAlt} />
            </span>
        </div>
    );
};

export default request;