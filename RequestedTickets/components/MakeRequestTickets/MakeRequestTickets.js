import React from 'react';

import './MakeRequestTickets.scss';

const makeRequestTickets = (props) =>  (
    <div className="make-request">
        <h4>Request Tickets</h4>
        <div className="request content-rounded">
            <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Ab aliquid dicta enim esse harum illum nostrum, nulla optio repudiandae?
                Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Ab aliquid dicta enim esse harum illum nostrum, nulla optio repudiandae?
            </span>
            <button onClick={props.click}>Make request</button>
        </div>
    </div>
);


export default makeRequestTickets;

