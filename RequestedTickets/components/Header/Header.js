import React from "react";

import Header from '../../../Profile/components/Header/Header';

const header = props => <Header isAuth={props.isAuth} />;

export default header;