import React from 'react';

import './AutoComplete.css';

const autoComplete = props => {

    const autoComplite = props.data.map((event,key) => (
        <div onClick={() => props.selectEventHandler(event)}
            key={key}
            className="row auto-complete-result-request">
            <div className="col-md-12">
                <h3 className="event-name">{event.name}</h3>
                <span className="city-name">{event.city}</span>
            </div>
        </div>
    ));

    let showAutoComplete = null;
    if (props.show) {
        showAutoComplete = (
            <div className="search-auto-complete">
                {autoComplite}
            </div>
        );
    }

    return showAutoComplete;
};

export default autoComplete;