import React from 'react';
import AuxFragment from "../../../../../hoc/AuxFragment";

const ticketTypes = ({ticket_types: ticketTypes, selectTicketTypesHandler}) => {
    console.log(ticketTypes);

    const types = ticketTypes.map(option => (
        <option key={option.id} value={option.id}>
            {option.name}
        </option>
    ));

    return (
        <AuxFragment>
            <span className="col-name">Ticket Types</span>
            <select
                className="select-ticket"
                onChange={selectTicketTypesHandler}>
                <option value="0">
                    I wish to receive all kinds of tickets
                </option>
                {types}
            </select>
        </AuxFragment>
    );
};

export default ticketTypes;