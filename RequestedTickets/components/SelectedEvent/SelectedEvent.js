import React from 'react';
import AuxFragment from "../../../../../hoc/AuxFragment";

import "./SelectedEvent.scss";
import {FormattedDate} from "react-intl";

const amount = props => {

    const {
        name: eventName,
        venue: {
            name: venueName
        },
        price_max: eventPrice,
        date: eventDate,
        country: {
            currency_symbol
        }
    } = props.event;

    const d = new Date(eventDate);
    const dateFormat = <FormattedDate
        value={d}
        year='numeric'
        month='long'
        day='2-digit' />;

    const currencySymbol = currency_symbol ? currency_symbol : null;

    let artist = null;
    if(props.event.artists){
        artist = props.event.artists.map(item => item.name);
    }

    const priceValue = `${currencySymbol} ${eventPrice}`;

    return (
        <AuxFragment>
            <span className="event-name">{eventName}</span>
            <div className="item-block">
                <span className="title-name">Event Venue</span>
                <input className="input-type" type="text" defaultValue={venueName} disabled/>
            </div>
            <div className="item-block">
                <span className="title-name">Event Date</span>
                <span className="type-date">{dateFormat}</span>
            </div>
            <div className="item-block">
                <span className="title-name">Max Price</span>
                <input className="input-type" type="text" defaultValue={priceValue} disabled/>
            </div>
            <div className="item-block">
                <span className="title-name">Artist</span>
                <input className="input-type" type="text" defaultValue={artist} disabled/>
            </div>
        </AuxFragment>
    );
}


export default amount;