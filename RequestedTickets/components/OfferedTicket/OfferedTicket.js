import React from 'react';
import {Link} from "react-router-dom";
import {FormattedDate} from "react-intl";

import './OfferedTicket.scss';
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {faTrashAlt} from "@fortawesome/fontawesome-free-solid";

const offeredTicket = props => {
    // console.log('offeredTicket', props);

    const {
        event: {
            name: eventName,
            city: cityName,
            venue: {
                name: venueName
            },
            venue_id: venueId
        },
        type: {
            name: typeName
        },
        ticket: {
            client: {
                name: nameClient,
                image_url: imageClient
            },
            amount: amountTicket
        },
        client_sold_tickets_count: soldTicket
    } = props.ticket;



    const d = new Date(props.ticket.event.date);
    const day = <FormattedDate
        value={d}
        day='2-digit' />;
    const year = <FormattedDate
        value={d}
        year='numeric'/>;
    const month = <FormattedDate
        value={d}
        month='short'/>;

    const seatsInfo = (ring, section, row, seat) => {

        ring = !ring ? null : `Ring ${ring}`;
        section = !section ? null : `Section ${section}`;
        row = !row ? null : `Row ${row}`;
        seat = !seat ? null : `Seat ${seat}`;

        const ticketData = [
            ring,
            section,
            row,
            seat
        ];
        return ticketData.filter(e => e === 0 || e ).join(' - ');
    }

    const ticketInfo = seatsInfo(props.ticket.ticket.tier, props.ticket.ticket.sector_name, props.ticket.ticket.row, props.ticket.ticket.seat);

    return (
            <div className="row responded-ticket">
                <div className="col-3 col-sm-2 col-md-2 col-lg-2 col-xl-2 date">
                    <div className="day">{day}</div>
                    <div className="month">{month}</div>
                    <div className="year">{year}</div>
                </div>
                <div className="col-8 col-sm-7 col-md-7 col-lg-7 col-xl-7">
                    <h3 className="title"><span className="type-ticket">{typeName}</span> {eventName}</h3>
                    <div className="location"><Link to={`/venue/${venueId}`}>@ {venueName} {cityName ? `, ${cityName}` : ''}</Link></div>
                    <hr/>
                    <div className="row">
                        <div className="col-sm-12 col-md-12 col-lg-7 col-xl-7">
                            <div className="title-info">Ticket information</div>
                            <div className="ticket-info">{ticketInfo}</div>
                        </div>
                        <div className="d-none d-sm-none d-lg-block col-lg-5 seller">
                            <img src={imageClient} alt="" />
                            <div className="about-seller">
                                <span className="name">{nameClient}</span>
                                <span className="number-tickets"> {soldTicket} Tickets sold</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3 text-right">
                    <div className="price-starts">Tickets price</div>
                    <div className="price"> &euro; {amountTicket}</div>
                    <div>
                        <button onClick={() => props.addToCartHandler(props.ticket, props.ticket.event)} className="goToEvent">Buy Ticket</button>
                    </div>
                </div>
                <span className="trash" style={{cursor:"pointer"}} onClick={() => props.click('remove', props.ticket.id)}>
                <FontAwesomeIcon
                    icon={faTrashAlt} />
            </span>
            </div>
    );
};

export default offeredTicket;