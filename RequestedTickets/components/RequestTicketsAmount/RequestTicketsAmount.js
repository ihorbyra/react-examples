import React from 'react';
import AuxFragment from "../../../../../hoc/AuxFragment";

const amount = props => (

    <AuxFragment>
        <span className="col-name">Amount</span>
        <input
            onChange={props.setAmountHandler}
            type="text"
            value={props.amount}
            className="amount-field"
            placeholder="Enter amount of wanted tickets"
        />
    </AuxFragment>

)


export default amount;