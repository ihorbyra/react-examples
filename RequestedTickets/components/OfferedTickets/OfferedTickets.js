import React from 'react';
import OfferedTicket from "../OfferedTicket/OfferedTicket";

const offeredTickets = props => {
    return props.offeredTickets.map((ticket, key) =>
        <OfferedTicket
            key={key}
            ticket={ticket}
            click={props.click}
            removeOfferedTicketHandler={props.removeOfferedTicketHandler}
            addToCartHandler={props.addToCartHandler}
        />
    );
};

export default offeredTickets;