import React from 'react';
import Request from "../Request/Request";

const requests = props => {
    return props.requests.map((request, key) =>
        <Request
            key={key}
            request={request}
            click={props.click}
            removeRequestHandler={props.removeRequestHandler}
        />
    );
};

export default requests;