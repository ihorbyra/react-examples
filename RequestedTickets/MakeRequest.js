import React, {Component} from 'react';
import {connect} from "react-redux";

import './RequestedTickets.scss';
import Header from './components/Header/Header';
import Sidebar from '../../Profile/Profile/components/Sidebar/Sidebar';
import AuxFragment from "../../../hoc/AuxFragment";
import AutoComplete from "./components/AutoComplete/AutoComplete";
import TicketTypes from "./components/TicketTypes/TicketTypes";
import RequestTicketsAmount from "./components/RequestTicketsAmount/RequestTicketsAmount";
import SelectedEvent from "./components/SelectedEvent/SelectedEvent";
import Button from '../../../components/UI/Button/Button';
import Modal from "../../../components/UI/Modal/Modal";
import * as actions from "../../../store/actions";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {faArrowLeft} from "@fortawesome/fontawesome-free-solid";

class MakeRequest extends Component {

    state = {
        startingSearch: false,
        searchQuery: '',
        selectedEvent: null,
        selectedTicketType: 0,
        amount: 1,
        successMsg: null,
        showModal: false
    };

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.hasSaved) {
            return {
                successMsg: nextProps.successMsg,
                showModal: true
            };
        }
        return null;
    }

    showModalHandler = () => {
        this.setState({
            ...this.state,
            showModal: true
        });
    }

    hideModalHandler = async () => {
        await this.props.onHasSavedToDefault();
        await this.setState({
            ...this.state,
            showModal: false,
            selectedEvent: null,
            selectedTicketType: 0,
            amount: 0,
            successMsg: null
        });
        this.props.history.replace(`/profile/requested-tickets`);
    }

    selectEventsHandler = event => {
        if (event.target.value.length >= 3) {

            const initialFilters = {
                filters:{"name": event.target.value}
            };
            this.props.onFetchEvents(initialFilters);

            if (!this.props.events.length) {
                this.setState({
                    ...this.state,
                    startingSearch: false,
                    searchQuery: event.target.value
                });
            } else {
                this.setState({
                    ...this.state,
                    startingSearch: true,
                    searchQuery: event.target.value
                });
            }
        } else {
            this.setState({
                ...this.state,
                startingSearch: false,
                searchQuery: null
            });
            this.props.onFetchEvents(null);
        }
    }

    selectEventHandler = event => {
        if (event !== null && event !== undefined) {
            this.setState({
                ...this.state,
                selectedEvent: event,
                startingSearch: false
            });
        }
    }

    selectTicketTypesHandler = event => {
        this.setState({
            ...this.state,
            selectedTicketType: event.target.value
        });
    }

    setAmountHandler = event => {
        this.setState({
            ...this.state,
            amount: event.target.value
        });
    }

    makeRequestHandler = () => {
        const data = {
            "event_id": this.state.selectedEvent.id,
            "type_id": this.state.selectedTicketType,
            "quantity": this.state.amount
        };
        this.props.onMakeRequest(data);
    }

    goBack = () => this.props.history.goBack();

    render() {
        let ticketTypes = null;
        let amount = null;
        let selectedEvent = null;
        let button = null;

        if (this.state.selectedEvent !== null && this.state.selectedEvent !== undefined) {
            ticketTypes = <TicketTypes
                {...this.state.selectedEvent}
                selectTicketTypesHandler={this.selectTicketTypesHandler}
            />;

            amount = <RequestTicketsAmount amount={this.state.amount} setAmountHandler={this.setAmountHandler} />;
            selectedEvent = <SelectedEvent event={this.state.selectedEvent} />;
            button = <Button
                clicked={this.makeRequestHandler}
                disabled={this.state.amount <= 0}
            >
                Request Ticket
            </Button>
        }

        return (
            <AuxFragment>
                <Modal show={this.state.showModal} modalClosed={this.hideModalHandler}>
                    <div className="modal-save-profile modal-class-success">
                        <div className="success-msg"><h5>{this.state.successMsg}</h5></div>
                        <span className="modal-close-but" onClick={this.hideModalHandler}></span>
                    </div>
                </Modal>
                <Header isAuth={this.props.isAuthentificated} />

                <div className="container profile-page ask-tickets">
                    <div className="row">
                        <Sidebar />
                        <div className="col-sm-12 col-md-12 col-lg-9 request-block">
                            <h4>Requested tickets</h4>
                            <div onClick={this.goBack} className="btn-back blue-color">
                                <i><FontAwesomeIcon icon={faArrowLeft}/></i>
                                Back
                            </div>
                            <div className="make-request-ticket content-rounded">
                                <div className="type-row">
                                    <span className="col-name">Event</span>
                                    <input
                                        onChange={this.selectEventsHandler}
                                        type="text"
                                        className="ticket-name"
                                        placeholder="Enter event name please"
                                    />

                                    <AutoComplete
                                        show={this.state.startingSearch}
                                        data={this.props.events}
                                        selectEventHandler={this.selectEventHandler}
                                    />

                                </div>
                                <div className="select-ticket">
                                    {selectedEvent}
                                </div>
                                <div className="type-ticket">
                                    {ticketTypes}
                                </div>
                                <div className="amount-ticket" >
                                    {amount}
                                </div>
                            </div>
                            <div className="btn-right">
                                {button}
                            </div>
                        </div>
                    </div>
                </div>
            </AuxFragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthentificated: state.auth.isAuth === true,
        events: state.events.events,
        hasSaved: state.calls.hasSaved,
        successMsg: state.calls.successMsg
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchEvents: filters => dispatch(actions.fetchEvents(null, filters)),
        onMakeRequest: data => dispatch(actions.makeRequestInit(data)),
        onHasSavedToDefault: () => dispatch(actions.setHasSavedToDefault())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(MakeRequest);