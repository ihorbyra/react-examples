import React, {Component} from 'react';
import {connect} from "react-redux";

import './RequestedTickets.scss'
import Header from './components/Header/Header';
import Modal from "../../../components/UI/Modal/Modal";
import Sidebar from '../../Profile/Profile/components/Sidebar/Sidebar';
import AuxFragment from "../../../hoc/AuxFragment";
import Requests from './components/Requests/Requests';
import MakeRequestTickets from './components/MakeRequestTickets/MakeRequestTickets';
import * as actions from "./store/actions";

class RequestedTickets extends Component {

    state = {
        showModal: false,
        selectId: null
    };

    componentDidMount() {
        this.props.onFetchRequests();
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.hasRemoved) {
            nextProps.onHasRemovedToDefault();
            nextProps.onFetchRequests();
        }
        return null;
    }

    showModalHandler = id => {
        this.setState({
            showModal: true,
            selectId: id
        });
    }

    hideModalHandler = () => {
        this.setState({
            showModal: false,
            selectId: null
        });
    }

    makeRequestHandler = () => {
        this.props.history.replace(`/profile/requested-tickets/make`);
    }

    removeRequestHandler = request => {
        this.setState({
            showModal: false
        });
        this.props.onRemoveRequest(request);
    }


    render() {
        let requests = null;
        if (this.props.requests.length) {
            requests = <Requests
                requests={this.props.requests}
                click={this.showModalHandler}
            />;
        }

        return (
            <AuxFragment>
                <Header isAuth={this.props.isAuthentificated} />
                <Modal show={this.state.showModal}>
                    <div className="modal-save-profile modal-class-success">
                        <div className="success-msg"><h5>Are you sure you want to remove?</h5></div>
                        <button onClick={this.hideModalHandler} className="close-btn">Close</button>
                        <button onClick={() => this.removeRequestHandler(this.state.selectId)} className="confirm-btn">Yes</button>
                        <span className="modal-close-but" onClick={this.hideModalHandler}></span>
                    </div>
                </Modal>
                <div className="container profile-page requested-tickets">
                    <div className="row">
                        <Sidebar />
                        <div className="col-sm-12 col-md-12 col-lg-9">
                            <MakeRequestTickets click={this.makeRequestHandler}/>
                            <h4>Your Requested tickets</h4>
                            {requests}
                        </div>
                    </div>
                </div>
            </AuxFragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthentificated: state.auth.isAuth === true,
        requests: state.calls.calls,
        hasRemoved: state.calls.hasRemoved
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchRequests: () => dispatch(actions.fetchOpenCallsInit()),
        onRemoveRequest: request => dispatch(actions.removeOpenCallsInit(request)),
        onHasRemovedToDefault: () => dispatch(actions.setHasRemovedOpenCallsToDefault())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(RequestedTickets);