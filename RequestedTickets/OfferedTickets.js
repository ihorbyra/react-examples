import React, {Component} from 'react';
import {connect} from "react-redux";
import {Link} from 'react-router-dom';

import Header from './components/Header/Header';
import './RequestedTickets.scss'
import Sidebar from '../../Profile/Profile/components/Sidebar/Sidebar';
import Modal from "../../../components/UI/Modal/Modal";
import AuxFragment from "../../../hoc/AuxFragment";
import OfferedTicketsList from './components/OfferedTickets/OfferedTickets';
import * as actions from "../../../store/actions";
import BuyTicketButtons from "../../Event/components/BuyTicketButtons/BuyTicketButtons";
import CountryConflict from "../../Event/components/CountryConflict/CountryConflict";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {faAngleLeft} from "@fortawesome/fontawesome-free-solid";

class OfferedTickets extends Component {

    state = {
        showModal: false,
        cart: {
            countryConflict: false
        },
        selectId: null,
        isBuyModal: false
    };

    componentDidMount() {
        this.props.onFetchOfferedTickets(this.offeredTicketId());
    }

    async componentDidUpdate(prevProps) {
        if (this.props.hasDeclined) {
            await this.props.onHasDeclinedToDefault();
            await this.props.onFetchOfferedTickets(this.offeredTicketId());
        }
    }

    offeredTicketId = () => this.props.match.params.id;

    showModalHandler = (type = "buy", id = null) => {
        let isBuyModal = false;
        if(type === "buy") {
            isBuyModal = true
        }

        this.setState({
            ...this.state,
            showModal: true,
            isBuyModal,
            selectId: id
        });
    }

    closeModalHandler = () => {
        this.setState({
            ...this.state,
            showModal: false,
            selectId: null,
            cart: {
                countryConflict: false
            }
        });
    }

    removeOfferedTicketHandler = ticket => {
        this.setState({
            ...this.state,
            showModal: false,
            selectId: null,
            cart: {
                countryConflict: false
            }
        });
        this.props.onRemoveOfferedTicket(ticket);
    }

    addToCartHandler = async (ticket, event) => {
        const country = await localStorage.getItem('cart_country');

        if (!country) {
            await localStorage.setItem('cart_country', event.country_id);
        }

        if (country && parseInt(country, 10) !== event.country_id) {
            this.setState({
                ...this.state,
                showModal: true,
                isBuyModal: true,
                cart: {
                    countryConflict: true
                }
            });

        } else {
            if (!this.checkingDubbing(ticket.ticket.id)) {
                const newTicket = {
                    ...ticket.ticket,
                    type: ticket.type
                };
                await this.props.onStoreShoppingCart(newTicket, event);
                await this.showModalHandler("buy");
                await this.props.onFetchOfferedTickets(this.offeredTicketId());
            }
        }
    }

    checkingDubbing = ticket => {
        let cart = localStorage.getItem('cart');
        if (!cart) return false;

        cart = JSON.parse(cart);
        if (cart.length) {
            return cart.some(item => item.id === ticket);
        }
        return false;
    }

    checkout = () => {
        this.props.history.replace(`/shopping-cart`);
    }

    render() {

        let offeredTickets = null;
        if (this.props.offeredTickets.length) {
            offeredTickets = <OfferedTicketsList
                offeredTickets={this.props.offeredTickets}
                removeOfferedTicketHandler={this.removeOfferedTicketHandler}
                addToCartHandler={this.addToCartHandler}
                click={this.showModalHandler}
            />;
        }

        let modalData = <BuyTicketButtons
            closeModalHandler={this.closeModalHandler}
            checkout={this.checkout}
        />;


        let removeTicket = (
            <div className="modal-save-profile modal-class-success">
                <div className="success-msg"><h5>Are you sure you want to remove?</h5></div>
                <button onClick={this.closeModalHandler} className="close-btn">Close</button>
                <button onClick={() => this.removeOfferedTicketHandler(this.state.selectId)} className="confirm-btn">Yes</button>
                <span className="modal-close-but" onClick={this.closeModalHandler}></span>
            </div>
        );

        if (this.state.cart.countryConflict) {
            modalData = <CountryConflict
                closeModalHandler={this.closeModalHandler}
                checkout={this.checkout}
            />;
        }

        return (
            <AuxFragment>
                <Modal show={this.state.showModal} modalClosed={this.closeModalHandler}>
                    {this.state.isBuyModal ? modalData : removeTicket}
                </Modal>
                <Header isAuth={this.props.isAuthentificated} />

                <div className="container profile-page ask-tickets">
                    <div className="row">
                        <Sidebar />
                        <div className="col-sm-12 col-md-12 col-lg-9" style={{paddingRight: '0'}}>
                            <h1 style={{marginBottom: '30px'}}>Responded tickets</h1>
                            <Link to={`/profile/requested-tickets/`} className="btn-back blue-color" style={{cursor: `pointer`, color: ``}}>
                                <i style={{paddingRight: '10px'}}><FontAwesomeIcon icon={faAngleLeft}/></i>
                                Back
                            </Link>
                            {offeredTickets}
                        </div>
                    </div>
                </div>
            </AuxFragment>
        );
    }
}

const mapStateToProps = state => {
    return {
        isAuthentificated: state.auth.isAuth === true,
        offeredTickets: state.calls.offeredTickets,
        hasDeclined: state.calls.hasDeclined
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onFetchOfferedTickets: event => dispatch(actions.fetchOfferedCallsInit(event)),
        onRemoveOfferedTicket: ticket => dispatch(actions.declineOfferedCallsInit(ticket)),
        onHasDeclinedToDefault: () => dispatch(actions.setHasDeclinedOpenedCallsToDefault()),

        onStoreShoppingCart: (ticket, country, status) => dispatch(actions.storeShoppingCartInit(ticket, country, status)),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(OfferedTickets);