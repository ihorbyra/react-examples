import * as actionTypes from './actionTypes';

export const fetchOpenCallsInit = () => {
    return {
        type: actionTypes.FETCH_CALLS_INIT,
    };
};

export const fetchOpenCallsStart = () => {
    return {
        type: actionTypes.FETCH_CALLS_START
    };
};

export const fetchOpenCallsSuccess = calls => {
    return {
        type: actionTypes.FETCH_CALLS_SUCCESS,
        calls
    };
};

export const fetchOpenCallsFail = error => {
    return {
        type: actionTypes.FETCH_CALLS_FAIL,
        error: error
    };
};

export const makeRequestInit = data => {
    return {
        type: actionTypes.MAKE_REQUEST_INIT,
        data
    };
};

export const makeRequestStart = () => {
    return {
        type: actionTypes.MAKE_REQUEST_START
    }
};

export const makeRequestSuccess = msg => {
    return {
        type: actionTypes.MAKE_REQUEST_SUCCESS,
        successMsg: msg
    };
};

export const makeRequestFail = error => {
    return {
        type: actionTypes.MAKE_REQUEST_FAIL,
        errorMsg: error
    };
};

export const setHasSavedToDefault = () => {
    return {
        type: actionTypes.SET_HAS_SAVED_TO_DEFAULT
    };
};

export const removeOpenCallsInit = request => {
    return {
        type: actionTypes.REMOVE_CALLS_INIT,
        request
    };
};

export const removeOpenCallsStart = () => {
    return {
        type: actionTypes.REMOVE_OPEN_CALLS_START
    }
};

export const removeOpenCallsSuccess = msg => {
    return {
        type: actionTypes.REMOVE_OPEN_CALLS_SUCCESS,
        successMsg: msg
    };
};

export const removeOpenCallsFail = error => {
    return {
        type: actionTypes.REMOVE_OPEN_CALLS_FAIL,
        errorMsg: error
    };
};

export const setHasRemovedOpenCallsToDefault = () => {
    return {
        type: actionTypes.SET_OPEN_CALLS_HAS_REMOVED_TO_DEFAULT
    };
};

export const fetchOfferedCallsInit = event => {
    return {
        type: actionTypes.FETCH_OFFERED_CALLS_INIT,
        event
    };
};

export const fetchOfferedCallsStart = () => {
    return {
        type: actionTypes.FETCH_OFFERED_CALLS_START
    };
};

export const fetchOfferedCallsSuccess = offeredTickets => {
    return {
        type: actionTypes.FETCH_OFFERED_CALLS_SUCCESS,
        offeredTickets
    };
};

export const fetchOfferedFail = error => {
    return {
        type: actionTypes.FETCH_OFFERED_CALLS_FAIL,
        error: error
    };
};

export const declineOfferedCallsInit = ticket => {
    return {
        type: actionTypes.DECLINE_OFFERED_CALLS_INIT,
        ticket
    }
};

export const declineOfferedCallsStart = () => {
    return {
        type: actionTypes.DECLINE_OFFERED_CALLS_START
    }
};

export const declineOfferedCallsSuccess = msg => {
    return {
        type: actionTypes.DECLINE_OFFERED_CALLS_SUCCESS,
        successMsg: msg
    };
};

export const declineOfferedCallsFail = error => {
    return {
        type: actionTypes.DECLINE_OFFERED_CALLS_FAIL,
        errorMsg: error
    };
};

export const setHasDeclinedOpenedCallsToDefault = () => {
    return {
        type: actionTypes.SET_OFFERED_CALLS_HAS_DECLINED_TO_DEFAULT
    };
};