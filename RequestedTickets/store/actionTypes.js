export const FETCH_CALLS_INIT = 'app/profile/requestedTickets/FETCH_CALLS_INIT';
export const FETCH_CALLS_START = 'app/profile/requestedTickets/FETCH_CALLS_START';
export const FETCH_CALLS_SUCCESS = 'app/profile/requestedTickets/FETCH_CALLS_SUCCESS';
export const FETCH_CALLS_FAIL = 'app/profile/requestedTickets/FETCH_CALLS_FAIL';

export  const MAKE_REQUEST_INIT = 'app/profile/requestedTickets/MAKE_REQUEST_INIT';
export  const MAKE_REQUEST_START = 'app/profile/requestedTickets/MAKE_REQUEST_START';
export  const MAKE_REQUEST_SUCCESS = 'app/profile/requestedTickets/MAKE_REQUEST_SUCCESS';
export  const MAKE_REQUEST_FAIL = 'app/profile/requestedTickets/MAKE_REQUEST_FAIL';

export const SET_HAS_SAVED_TO_DEFAULT = 'app/profile/requestedTickets/SET_HAS_SAVED_TO_DEFAULT';

export const REMOVE_CALLS_INIT = 'app/profile/requestedTickets/REMOVE_CALLS_INIT';
export const REMOVE_OPEN_CALLS_START = 'app/profile/requestedTickets/REMOVE_CALLS_START';
export const REMOVE_OPEN_CALLS_SUCCESS = 'app/profile/requestedTickets/REMOVE_CALLS_SUCCESS';
export const REMOVE_OPEN_CALLS_FAIL = 'app/profile/requestedTickets/REMOVE_CALLS_FAIL';

export const SET_OPEN_CALLS_HAS_REMOVED_TO_DEFAULT = 'app/profile/requestedTickets/SET_CALLS_HAS_REMOVED_TO_DEFAULT';

export const FETCH_OFFERED_CALLS_INIT = 'app/profile/requestedTickets/FETCH_OFFERED_CALLS_INIT';
export const FETCH_OFFERED_CALLS_START = 'app/profile/requestedTickets/FETCH_OFFERED_START';
export const FETCH_OFFERED_CALLS_SUCCESS = 'app/profile/requestedTickets/FETCH_OFFERED_SUCCESS';
export const FETCH_OFFERED_CALLS_FAIL = 'app/profile/requestedTickets/FETCH_OFFERED_FAIL';

export const DECLINE_OFFERED_CALLS_INIT = 'app/profile/requestedTickets/DECLINE_OFFERED_INIT';
export const DECLINE_OFFERED_CALLS_START = 'app/profile/requestedTickets/DECLINE_OFFERED_START';
export const DECLINE_OFFERED_CALLS_SUCCESS = 'app/profile/requestedTickets/DECLINE_OFFERED_SUCCESS';
export const DECLINE_OFFERED_CALLS_FAIL = 'app/profile/requestedTickets/DECLINE_OFFERED_FAIL';

export const SET_OFFERED_CALLS_HAS_DECLINED_TO_DEFAULT = 'app/profile/requestedTickets/SET_OFFERED_HAS_DECLINED_TO_DEFAULT';