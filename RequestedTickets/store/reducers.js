import * as actionTypes from './actionTypes';
import { updateObject } from '../../../../shared/utilities';

const initialState = {
    calls: [],
    offeredTickets: [],
    loading: false,
    hasRemoved: false,
    hasSaved: false,
    hasDeclined: false,
    successMsg: null,
    errorMsg: null
};

const fetchOpenCallsStart = state => {
    return updateObject(state, { loading: true });
};

const fetchOpenCallsSuccess = (state, action) => {
    return updateObject( state, {
        calls: action.calls,
        loading: false
    });
};

const fetchOpenCallsFail = state => {
    return updateObject(state, { loading: false });
};

const makeRequestStart = state => {
    return updateObject(state, { loading: true });
};

const makeRequestSuccess = (state, action) => {
    return updateObject( state, {
        loading: false,
        hasSaved: true,
        successMsg: action.successMsg
    });
};

const makeRequestFail = (state, action) => {
    return updateObject(state, {
        loading: false,
        errorMsg: action.errorMsg
    });
};

const setHasSavedToDefault = state => {
    return updateObject(state, {
        hasSaved: false,
        successMsg: null,
        errorMsg: null
    });
};

const removeOpenCallsStart = state => {
    return updateObject(state, { loading: true });
};

const removeOpenCallsSuccess = (state, action) => {
    return updateObject( state, {
        loading: false,
        hasRemoved: true,
        successMsg: action.successMsg
    });
};

const removeOpenCallsFail = (state, action) => {
    return updateObject(state, {
        loading: false,
        errorMsg: action.errorMsg
    });
};

const setHasRemoveOpenCallToDefault = state => {
    return updateObject(state, {
        hasRemoved: false,
        successMsg: null,
        errorMsg: null
    });
};

const fetchOfferedCallsStart = state => {
    return updateObject(state, { loading: true });
};

const fetchOfferedCallsSuccess = (state, action) => {
    return updateObject( state, {
        offeredTickets: action.offeredTickets,
        loading: false
    });
};

const fetchOfferedCallsFail = state => {
    return updateObject(state, { loading: false });
};

const declineOfferedCallsStart = state => {
    return updateObject(state, { loading: true });
};

const declineOfferedCallsSuccess = (state, action) => {
    return updateObject( state, {
        loading: false,
        hasDeclined: true,
        successMsg: action.successMsg
    });
};

const declineOfferedCallsFail = (state, action) => {
    return updateObject(state, {
        loading: false,
        errorMsg: action.errorMsg
    });
};

const setHasDeclinedOfferedCallToDefault = state => {
    return updateObject(state, {
        hasDeclined: false,
        successMsg: null,
        errorMsg: null
    });
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.FETCH_CALLS_START: return fetchOpenCallsStart(state);
        case actionTypes.FETCH_CALLS_SUCCESS: return fetchOpenCallsSuccess(state, action);
        case actionTypes.FETCH_CALLS_FAIL: return fetchOpenCallsFail(state);

        case actionTypes.MAKE_REQUEST_START: return makeRequestStart(state);
        case actionTypes.MAKE_REQUEST_SUCCESS: return makeRequestSuccess(state, action);
        case actionTypes.MAKE_REQUEST_FAIL: return makeRequestFail(state, action);
        case actionTypes.SET_HAS_SAVED_TO_DEFAULT: return setHasSavedToDefault(state);

        case actionTypes.REMOVE_OPEN_CALLS_START: return removeOpenCallsStart(state);
        case actionTypes.REMOVE_OPEN_CALLS_SUCCESS: return removeOpenCallsSuccess(state, action);
        case actionTypes.REMOVE_OPEN_CALLS_FAIL: return removeOpenCallsFail(state, action);
        case actionTypes.SET_OPEN_CALLS_HAS_REMOVED_TO_DEFAULT: return setHasRemoveOpenCallToDefault(state);

        case actionTypes.FETCH_OFFERED_CALLS_START: return fetchOfferedCallsStart(state);
        case actionTypes.FETCH_OFFERED_CALLS_SUCCESS: return fetchOfferedCallsSuccess(state, action);
        case actionTypes.FETCH_OFFERED_CALLS_FAIL: return fetchOfferedCallsFail(state);

        case actionTypes.DECLINE_OFFERED_CALLS_START: return declineOfferedCallsStart(state);
        case actionTypes.DECLINE_OFFERED_CALLS_SUCCESS: return declineOfferedCallsSuccess(state, action);
        case actionTypes.DECLINE_OFFERED_CALLS_FAIL: return declineOfferedCallsFail(state, action);
        case actionTypes.SET_OFFERED_CALLS_HAS_DECLINED_TO_DEFAULT: return setHasDeclinedOfferedCallToDefault(state);

        default: return state;
    }
};

export default reducer;
