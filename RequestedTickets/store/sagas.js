import {put, call, takeEvery, takeLatest} from 'redux-saga/effects';
import http from "../../../../http/axiosInstance";

import * as actionTypes from './actionTypes';
import * as actions from './actions';

export function* fetchRequestedTicketsWatcher() {
    yield takeEvery(actionTypes.FETCH_CALLS_INIT, fetchRequestedTickets);
}

export function* makeRequestWatcher() {
    yield takeLatest(actionTypes.MAKE_REQUEST_INIT, makeRequest);
}

export function* removeRequestedTicketsWatcher() {
    yield takeLatest(actionTypes.REMOVE_CALLS_INIT, removeRequestedTickets);
}

export function* fetchOfferedTicketsWatcher() {
    yield takeEvery(actionTypes.FETCH_OFFERED_CALLS_INIT, fetchOfferedTickets);
}

export function* declineOfferedTicketsWatcher() {
    yield takeEvery(actionTypes.DECLINE_OFFERED_CALLS_INIT, declineOfferedTickets);
}

function* fetchRequestedTickets() {

    try {

        yield put(actions.fetchOpenCallsStart());

        const token = yield call(() => localStorage.getItem('token'));
        const config = {
            transformRequest: [(data, headers) => {
                headers.authorization = `Bearer ${token}`;
                headers['Content-Type'] = 'application/json';
                return JSON.stringify(data);
            }]
        };

        const response = yield call(() =>  http.get(`/v1/ticket/calls`, config));
        const responseData = response.data.data;
        yield put(actions.fetchOpenCallsSuccess(responseData));


    } catch (error) {
        yield put(actions.fetchOpenCallsFail(error.response.data.error.message));
    }
}

function* makeRequest(action) {
    try {
        yield put(actions.makeRequestStart());

        const token = yield call(() => localStorage.getItem('token'));
        const config = {
            transformRequest: [(data, headers) => {
                headers.authorization = `Bearer ${token}`;
                headers['Content-Type'] = 'application/json';
                return JSON.stringify(data);
            }]
        };

        const response = yield call(() =>  http.post(`/v1/ticket/request`, action.data, config));
        const responseData = response.data.data.success;
        yield put(actions.makeRequestSuccess(responseData));


    } catch (error) {
        yield put(actions.makeRequestFail(error.response.data.error.message));
    }
}

function* removeRequestedTickets(action) {

    try {
        yield put(actions.removeOpenCallsStart());

        const token = yield call(() => localStorage.getItem('token'));
        const config = {
            transformRequest: [(data, headers) => {
                headers.authorization = `Bearer ${token}`;
                headers['Content-Type'] = 'application/json';
                return JSON.stringify(data);
            }]
        };

        const response = yield call(() =>  http.delete(`/v1/ticket/request/${action.request}`, config));
        const responseData = response.data.data.success;
        yield put(actions.removeOpenCallsSuccess(responseData));


    } catch (error) {
        yield put(actions.removeOpenCallsFail(error.response.data.error.message));
    }
}

function* fetchOfferedTickets(action) {

    try {

        yield put(actions.fetchOfferedCallsStart());

        const token = yield call(() => localStorage.getItem('token'));
        const config = {
            transformRequest: [(data, headers) => {
                headers.authorization = `Bearer ${token}`;
                headers['Content-Type'] = 'application/json';
                return JSON.stringify(data);
            }]
        };

        const response = yield call(() =>  http.get(`/v1/ticket/offered/${action.event}`, config));
        const responseData = response.data.data;
        yield put(actions.fetchOfferedCallsSuccess(responseData));


    } catch (error) {
        yield put(actions.fetchOfferedFail(error.response.data.error.message));
    }
}

function* declineOfferedTickets(action) {
    try {
        yield put(actions.declineOfferedCallsStart());

        const token = yield call(() => localStorage.getItem('token'));
        const config = {
            transformRequest: [(data, headers) => {
                headers.authorization = `Bearer ${token}`;
                headers['Content-Type'] = 'application/json';
                return JSON.stringify(data);
            }]
        };

        const response = yield call(() =>  http.put(`/v1/ticket/offered/${action.ticket}`, config));
        const responseData = response.data.data.success;
        yield put(actions.declineOfferedCallsSuccess(responseData));


    } catch (error) {
        yield put(actions.declineOfferedCallsFail(error.response.data.error.message));
    }
}

